package st.reiter.yahooClient;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Type;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;
/**
 * 
 * @author greiter
 *@see https://developer.yahoo.com/weather/
 */
public class YahooClient {

	private static String MAIN_URL ="http://query.yahooapis.com/v1/public/yql?q={yql_query}&format=json";
	
	public static final String PARAM_VIENNA = "Vienna, AT";
	public static final String PARAM_NEWYORK = "New York, US";
	public static final String PARAM_LONDON = "London, UK";
	
	public YahooClient()  {}
	
	public YahooResponse get(String query) throws IOException {
		
		String yql = "select * from weather.forecast where woeid in (select woeid from geo.places(1) where text=\""+query+"\") and u='c'";
		
		URL url = new URL(MAIN_URL.replace("{yql_query}", URLEncoder.encode(yql,"UTF-8")));
		
		 Reader reader = new InputStreamReader(url.openStream()); 
		 // Set custom Date Converter
        Gson gson = new GsonBuilder().registerTypeAdapter(Date.class,new dateConverter()).create();
        
        return gson.fromJson(reader, YahooResponse.class);

	}

	static private class dateConverter implements JsonDeserializer<Date> {

		public Date deserialize(JsonElement element, Type type, JsonDeserializationContext arg2)
				throws JsonParseException {

			try {
				return SDF.parse(element.getAsString());
			} catch (ParseException e) {
				try {
					return SDF_2.parse(element.getAsString());
				} catch (ParseException e1) {				
					try {
						return SDF_3.parse(element.getAsString());
					} catch (ParseException e2) {
						e2.printStackTrace();
					}
				}
			}

			return null;
		}
		
	}
	private static SimpleDateFormat SDF = new SimpleDateFormat("EEE, dd MMM yyyy hh:mm a z",Locale.US);
	private static SimpleDateFormat SDF_2 = new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss'Z'");
	private static SimpleDateFormat SDF_3 = new SimpleDateFormat("dd MMM yyyy",Locale.US);
}
