package st.reiter.yahooClient.pojo;

import java.io.Serializable;

public class Results implements Serializable {

	private static final long serialVersionUID = -2477020883851906347L;
	private Channel channel;

	public Channel getChannel() {
		return channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}
}
