package st.reiter.yahooClient.pojo;

import java.io.Serializable;
import java.util.Date;

import st.reiter.yahooClient.pojo.Results;

public class Query implements Serializable{

	private static final long serialVersionUID = -8415437350844224846L;
	private int count;
	private Date created;
	private Results results;
	
	
	
	public int getCount() {
		return count;
	}
	public void setCount(int count) {
		this.count = count;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	public Results getResults() {
		return results;
	}
	public void setResults(Results results) {
		this.results = results;
	}
	
	
}
