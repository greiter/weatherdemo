package st.reiter.yahooClient;

import java.io.Serializable;

import st.reiter.yahooClient.pojo.Query;

public class YahooResponse implements Serializable {

	private static final long serialVersionUID = -3030596573828410624L;
	private Query query;
	
	public Query getQuery() {
		return query;
	}
	
	public void setQuery(Query query) {
		this.query = query;
	}
	
}
