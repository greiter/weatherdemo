package st.reiter.beans;

import java.io.IOException;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import st.reiter.yahooClient.YahooClient;
import st.reiter.yahooClient.YahooResponse;
@ManagedBean
@ViewScoped
public class WeatherBean implements Serializable {

	private static final long serialVersionUID = 475815790537840691L;
	YahooResponse resp;
	
	public WeatherBean() {}

	 @PostConstruct
	 private void init() {
		 String city =  FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("city");
		
		 if (city==null) city = "vienna";
		 
		 try {
				switch(city){
				case "newyork":
					resp = new YahooClient().get(YahooClient.PARAM_NEWYORK);
					break;
				case "london":
					resp = new YahooClient().get(YahooClient.PARAM_LONDON);
					break;
				case "vienna":
				default:
						resp = new YahooClient().get(YahooClient.PARAM_VIENNA);
				}

			} catch (IOException e) {
				e.printStackTrace();
			}
	 
	}

	public YahooResponse getResp() {
		return resp;
	}

	public void setResp(YahooResponse resp) {
		this.resp = resp;
	}
	
}
